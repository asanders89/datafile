# Datafile

Datafile is a wrapper around [h5py](http://www.h5py.org/) to enable additional functionality and common data storage operations related to data acqusition such as dataset name incrementation. The module also provides utility functions for common file operations.

**What is HDF5 and why is it used during data acquisition?**

HDF5 is a common binary data storage formay found in data science applications due to its flexibility, storage effeciency, and features such as object attributes for metadata storage.

**What does Datafile do?**

The `DataFile` object wraps the `h5py.File`  class to add additional features and wraps any returned instances of `h5py.Group` and `h5py.Dataset` to enable similar functionality.

## Getting Started

### Prerequisites

[h5py](http://www.h5py.org/) is the only external dependency — see [here](https://github.com/h5py/h5py) for installation instructions.

### Installation

Installation is currently possible using setuptools and the included setup.py. Navigate to the repository and from the command line run:
`python setup.py install`

## Examples



## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/asanders89/datafile/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

- **Alan Sanders** ([asanders89](https://gitlab.com/asanders89))
- **Richard Bowman**

See also the list of [contributors](https://gitlab.com/asanders89/datafile/contributors) who participated in this project.

## License

This project is licensed under the MIT License — see the [LICENSE](https://gitlab.com/asanders89/datafile/LICENSE) file for details

## Acknowledgments

