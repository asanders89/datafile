"""
This module provides the DataFile class, a subclass of h5py's File class with a
extended functionality. Groups and Datasets returned by a DataFile are
subclassed h5py Groups and Datasets, again to facilitate extended functionality.

"""

import warnings
import re
from collections import Sequence
import datetime

import h5py

from .utils import inherit_docstring


def _wrap_h5py_item(item):
    """
    Wrap an h5py object: groups are returned as Group objects, datasets are
    returned as Dataset objects.

    Args:
        item: h5py object.

    Returns:
        Wrapped h5py object.

    """
    if isinstance(item, h5py.Group):
        return Group(item.id)
    elif isinstance(item, h5py.Dataset):
        return Dataset(item.id)
    else:
        return item


class Dataset(h5py.Dataset):
    """
    HDF5 Dataset - Wrapped h5py.Dataset to provide additional functionality
    such as appending data and metadata.

    """
    @property
    def parent(self):
        """Return the group to which this object belongs."""
        return _wrap_h5py_item(super().parent)

    def set_attrs_from_dict(self, attrs_dict):
        """
        Set the dataset's attributes from a dictionary of metadata.

        Args:
            attrs_dict (dict): dictionary of attributes.

        """
        for key, value in attrs_dict.items():
            if value is not None:
                try:
                    self.attrs[key] = value
                except TypeError:
                    msg = "Metadata {0}='{1}' can't be saved in HDF5. Saving " \
                          "as str()".format(key, value)
                    warnings.warn(msg)
                    self.attrs[key] = str(value).encode()

    def update_attrs(self, attrs_dict):
        """
        Update (create or modify) the attributes of this group.

        Args:
            attrs_dict (dict): dictionary of attributes.

        """
        self.set_attrs_from_dict(attrs_dict)

    def append(self, data, axis=0):
        """Resize the dataset along the selected axis and append new data
        values.

        Args:
            data (array-like):

        Keyword Args:
            axis (int): (default 0).
        """
        index = self.shape[axis]
        if hasattr(data, 'shape'):
            extend = data.shape[axis]
        elif hasattr(data, '__len__'):
            extend = len(data)
        else:
            extend = 1
        self.resize(index + extend, axis)
        self[index:,...] = data


class Group(h5py.Group):
    """
    HDF5 Group - a collection of datasets and subgroups. Wrapped h5py.Group
    to provide additional functionality such as dataset incrementation.

    """
    def __getitem__(self, key):
        item = super().__getitem__(key)  # get the dataset or group
        return _wrap_h5py_item(item)  # wrap if necessary

    @property
    def parent(self):
        """Return the group to which this object belongs."""
        return _wrap_h5py_item(super().parent)

    def set_attrs_from_dict(self, attrs_dict):
        """
        Set the group's attributes from a dictionary of metadata.

        Args:
            attrs_dict (dict): dictionary of attributes.

        """
        for key, value in attrs_dict.items():
            if value is not None:
                try:
                    self.attrs[key] = value
                except TypeError:
                    msg = "Metadata {0}='{1}' can't be saved in HDF5. Saving " \
                          "as str()".format(key, value)
                    warnings.warn(msg)
                    self.attrs[key] = str(value).encode()

    def update_attrs(self, attrs_dict):
        """
        Update (create or modify) the attributes of this group.

        Args:
            attrs_dict (dict): dictionary of attributes.

        """
        self.set_attrs_from_dict(attrs_dict)

    def find_unique_name(self, name, num_digits=2):
        """
        Find a unique name for a subgroup or dataset in this group.

        Args:
            name (str): If this contains a %d placeholder, it will be
                replaced with the lowest integer such that the new name is
                unique. If no %d is included, _%d will be appended to the
                name if the name already exists in this group.

        Keyword Args:
            num_digits (int): Minimum number of digits to include in numbering,
                i.e. 1 (1), 2 (01), or 3 (001).
                
        Returns:
            str: Next available uniquely numbered name.

        """
        incl_formatting = len(re.findall(r'{(.*?)}', name)) > 0
        if not incl_formatting and name not in self:
            return name  # simplest case: it's already a unique name
        else:
            if not incl_formatting:
                name += '_{{:0{:d}d}}'.format(num_digits)
            n = 1
            while name.format(n) in self:
                n += 1  # increase the number until the name is unique
            return name.format(n)

    @staticmethod
    def h5_item_number(h5item):
        """
        Returns the number at the end of a group/dataset name, or None.

        Args:
            h5item (h5py.Group or h5py.Dataset): h5py Object.

        Returns:
            int: item number.

        """
        m = re.search(r'(\d+)$', h5item.name)  # match numbers at the end of
        # the name
        return int(m[0]) if m else None

    def get_numbered_items(self, name):
        """
        Get a list of datasets/groups that have a given name, sorted by the
        number appended to the end.

        This function is intended to return items saved with
        auto_increment=True, in the order they were added (by default they
        come in alphabetical order, so 10 comes before 2).  `name` is the
        name passed in without the _0 suffix.
        
        Args:
            name (str): Name of the datasets/groups without the _{number}
                suffix.
            
        Returns:
            list: List of datasets/groups that have a given name and number,
                sorted by the number appended to the end.

        """
        items = [_wrap_h5py_item(v) for k,v in self.iteritems()
                 if k.startswith(name)  # only items that start with `name`
                 and re.match(r"_*(\d+)$", k[len(name):])]  # and end with numbers
        return sorted(items, key=self.h5_item_number)

    def count_numbered_items(self, name):
        """
        Count the number of items that would be returned by `get_numbered_items`.

        If all you need to do is count how many items match a name, this is
        a faster way to do it than len(group.get_numbered_items('name')).
        
        Args:
            name (str): Name of the datasets/groups without the _{number} suffix.
        
        Returns:
            int: Number of numbered items.
            
        """
        n = 0
        for k in self.keys():
            if k.startswith(name) and re.match(r"_*(\d+)$", k[len(name):]):
                n += 1
        return n

    @inherit_docstring(h5py.Group.create_group)
    def create_group(self, name, attrs=None, auto_increment=True,
                     timestamp=True, overwrite=False):
        """
        Create a new group, ensuring we don't overwrite old ones.

        A new group is created within this group, with the specified name.
        If auto_increment is True (the default) then a number is used to ensure
        the name is unique.

        Args:
            name (str): The name of the new group.  May contain a %d placeholder as described in 
                find_unique_name()

        Keyword Args:
            auto_increment (bool): True by default, which invokes the unique
                name behaviour described in find_unique_name. Set this to
                False to cause an error if the desired name exists already.
            timestamp (bool): Timestamp the group. Defaults to True.
            overwrite (bool): Overwrite a currently existing group of the
                same name.
            attrs (dict): Dictionary of attributes.
            
        Returns:
            Group: Wrapped h5py Group.
            
        """
        if overwrite and name in self:
            del self[name]
        if auto_increment and name is not None:
            name = self.find_unique_name(name)  # name is None if creating via the dict interface
        grp = super(Group, self).create_group(name)
        grp = Group(grp.id)  # wrap the group
        if timestamp:
            t = datetime.datetime.now().isoformat()
            grp.attrs.create('creation_timestamp', t.encode())
        if attrs is not None:
            grp.set_attrs_from_dict(attrs)
        return grp

    @inherit_docstring(h5py.Group.require_group)
    def require_group(self, name, attrs=None, auto_increment=True,
                      timestamp=True):
        """Return a subgroup, creating it if it does not exist."""
        if name not in self:
            grp = self.create_group(name, attrs, auto_increment, timestamp)
        else:
            grp = self[name]
        return grp
        #grp = super(Group, self).require_group(name)
        #return Group(grp.id)  # wrap the returned group

    @inherit_docstring(h5py.Group.create_dataset)
    def create_dataset(self, name, auto_increment=True, shape=None,
                       dtype=None, data=None, attrs=None, timestamp=True,
                       overwrite=False, resizeable=False, *args, **kwargs):
        """
        Create a new dataset, optionally with an auto-incrementing name.
        Further arguments are passed to h5py.Group.create_dataset.

        Args:
            name (str): the name of the new dataset

        Keyword Args:
            auto_increment (bool): if True (default), add a number to the
                dataset name to ensure it's unique. To force the addition of
                a number, append %d to the dataset name.
            shape (tuple): a tuple describing the dimensions of the data
                (only needed if data is not specified)
            dtype: data type to be saved (if not specifying data)
            data (array-like): a numpy array or equivalent, to be saved -
                this specifies dtype and shape.
            attrs (dict): a dictionary of metadata to be saved with the data
            timestamp (bool): if True (default), we save a
                "creation_timestamp" attribute with the current time.
            overwrite (bool): Overwrite a currently existing group of the
                same name.
            resizeable (bool): If True, create a resizeable dataset. This
                requires specifying.
            maxshape ((None,) if unlimited) and chunks=True. Default is False.

            *args: Variable length argument list passed to
                h5py.Group.create_dataset.
            **kwargs: Arbitrary keyword arguments passed to
                h5py.Group.create_dataset.
            
        Returns:
            Dataset: wrapped h5py Dataset.

        """
        if overwrite and name in self:
            del self[name]
        if auto_increment and name is not None:  # name is None if we are creating via the dict interface
            name = self.find_unique_name(name)
        dset_kwargs = kwargs
        if resizeable:
            if 'maxshape' not in kwargs:
                assert shape is not None, 'Shape must be specified for a ' \
                                          'resizeable array'
                kwargs['maxshape'] = (None,)*len(shape)
            dset_kwargs['chunks'] = True
            dset_kwargs['maxshape'] = kwargs['maxshape']
        dset = super(Group,self).create_dataset(name, shape, dtype, data,
                                                *args, **dset_kwargs)
        dset = Dataset(dset.id)  # wrap the dataset
        if timestamp:
            t = datetime.datetime.now().isoformat()
            dset.attrs.create('creation_timestamp', t.encode())
        attr_names = ['attrs', 'metadata', 'meta']
        for attr_name in attr_names:
            if hasattr(data, attr_name):
                dset.set_attrs_from_dict(getattr(data, attr_name))
        if attrs is not None:
            dset.set_attrs_from_dict(attrs)  # quickly set the attributes
        return dset

    @inherit_docstring(h5py.Group.require_dataset)
    def require_dataset(self, name, auto_increment=True, shape=None,
                        dtype=None, data=None, attrs=None, timestamp=True,
                        resizeable=False, *args, **kwargs):
        """Require a new dataset, optionally with an auto-incrementing name."""
        if name not in self:
            dset = self.create_dataset(name, auto_increment, shape, dtype,
                                       data, attrs, timestamp, resizeable,
                                       *args, **kwargs)
        else:
            dset = self[name]
        return dset

    def append_dataset(self, name, value, dtype=None):
        """
        Append the given data to an existing dataset, creating it if it
        doesn't exist.
        
        Args:
            name (str): Name of the dataset to append to.
            value (np.ndarray): Values to append.
            
        Keyword Args:
            dtype: Datatype if creating the dataset. Defaults to None
                (automatically determine dataset).
            
        """
        if name not in self:
            if hasattr(value, 'shape'):
                shape = (0,) + value.shape
                maxshape = (None,) + value.shape
            elif isinstance(value, Sequence):
                shape = (0, len(value))
                maxshape = (None, len(value))  # tuple(None for i in shape)
            else:
                shape = (0,)
                maxshape = (None,)
            dset = self.require_dataset(name, shape=shape, dtype=dtype,
                                        maxshape=maxshape, chunks=True)
        else:
            dset = self[name]
        index = dset.shape[0]
        dset.resize(index + 1, 0)
        dset[index,...] = value

    @property
    def basename(self):
        """
        Return the last part of self.name, i.e. just the final component of
        the path.
        """
        return self.name.rsplit("/", 1)[-1]


@inherit_docstring(h5py.File)
class DataFile(Group, h5py.File):
    """Wrapped h5py File class."""

    _current = None

    def __init__(self, name, mode=None, set_current=True, *args, **kwargs):
        if isinstance(name, h5py.File):  # wrap a provided file
            h5file = name
            name = h5file.filename
            h5file.close()
            # self = name #if it's already an open file, just use it
        super().__init__(name, mode, *args, **kwargs)
        if set_current or self._current is None:
            self.set_current()

    @classmethod
    def get_current(cls):
        """Get the current data file."""
        return cls._current

    def set_current(self):
        """Set this file as the default file for all new data."""
        self._current = self


if __name__ == '__main__':
    import numpy as np

    with DataFile('test.h5') as f:
        dset = f.require_dataset('test', shape=(0,2), dtype=int,
                                 resizeable=True, overwrite=True)
        for i in range(10):
            dset.append([[i,i]])
        print(dset.shape)
        print(dset[:])
