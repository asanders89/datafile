"""
## Utility/support functions

Utility functions to support data file usage.

"""
# TODO: compact get_dated_subdirectory and get_dated_subdirectories with
# multiple format options

import warnings
import re
from pathlib import Path
import datetime
import os


class inherit_docstring(object):
    """Appends the current functions docstring with that of the specified base function"""

    def __init__(self, base_f):
        self.base_f = base_f

    def __call__(self, f):
        # if the base function has no docstring then do nothing
        if self.base_f.__doc__ is None:
            return f
        # using new lines is not necessary if there is no current docstring to separate
        if f.__doc__ is None:
            f.__doc__ = self.base_f.__doc__
        elif f.__doc__ == '':
            f.__doc__ += self.base_f.__doc__
        # if there is a docstring in both the current function and the base function
        # then the new docstring will include both docstrings separated by an empty line
        else:
            f.__doc__ += '\n\n'+self.base_f.__doc__
        return f


def get_dated_subdirectory(base_directory):
    """
    Get a single dated subdirectory if the form of /year_month_day/,
    e.g. /2017_01_01/.

    Args:
        base_directory (str): path to main directory.

    Returns:
        Path: path to dated subdirectory.

    """
    date = datetime.datetime.now()
    subdir_structure = Path('{:04d}_{:02d}_{:02d}'.format(date.year, date.month, date.day))
    print(subdir_structure)
    dated_dir = Path(base_directory)/subdir_structure
    if not dated_dir.exists():
        os.makedirs(str(dated_dir))
    return dated_dir


def get_dated_subdirectories(base_directory):
    """
    Get a dated hierarchy of subdirectories if the form of
    /year/month number. month/day, e.g. /2017/01. Jan/01/.

    Args:
        base_directory (str): path to main directory.

    Returns:
        Path: path to dated subdirectory.

    """
    date = datetime.datetime.now()
    subdir_structure = Path(str(date.year),
                      '{:02d}'.format(date.month) + '. ' + date.strftime('%b'),
                      '{:02d}'.format(date.day))
    print(subdir_structure)
    dated_dir = Path(base_directory)/subdir_structure
    if not dated_dir.exists():
        os.makedirs(str(dated_dir))
    return dated_dir


def get_filename(base_dir, fname='data.h5'):
    """
    Creates a dated directory path and returns a file name to open a file
    there. Note that the file is not created in this method.

    Args:
        base_dir (str): path to main directory.
        fname (str): filename in dated subdirectory.

    Returns:
        Path: path to file.

    """
    data_dir = get_dated_subdirectory(base_dir)
    file_path = data_dir/fname
    return file_path


def get_unique_name(name, num_digits=2):
    """
    Get a unique path name for an item with a given base name by incrementing a
    number starting at 1.

    Args:
        basename (str): base name of the item to be incremented. Basename
            ideally contains '{:0xd}' where x is the significant figures for
            string formatting.

    Returns:
        Path: unique name based on incrementing base name in base_dir

    """
    if not isinstance(name, Path):
        name = Path(name)
    parent = name.parent
    base = name.name
    if len(re.findall(r'{(.*?)}', base)) == 0:
        components = base.split('.')
        components[0] += f'_{{:0{num_digits:d}d}}'
        base = '.'.join(components)
    name = str(parent/base)
    n = 1
    while os.path.exists(name.format(n)):
        n += 1
    return name.format(n)


def attributes_from_dict(h5item, dict_of_attributes):
    """
    Update the metadata of an HDF5 object with a dictionary.

    Args:
        h5item (h5py.Dataset or h5py.Group): h5py object to which attributes
            are attributed.
        dict_of_attributes (dict): dictionary of attributes.

    """
    attrs = h5item.attrs
    for key, value in dict_of_attributes.items():
        if value is not None:
            try:
                attrs[key] = value
            except TypeError:
                msg = "Metadata {0}='{1}' can't be saved in HDF5. Saving as " \
                      "str()".format(key, value)
                warnings.warn(msg)
                attrs[key] = str(value).encode()


if __name__ == '__main__':
    print(get_unique_name('~/Desktop/data.h5'))
    print(get_unique_name('~/Desktop/data_{:02d}.h5'))
    print(get_unique_name('~/Desktop/data'))
    print(get_unique_name('~/Desktop/data_{:02d}'))
