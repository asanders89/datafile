#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='datafile',
      version='0.1',
      description='Wrapper around h5py objects to provide extended '
                  'functionality when acquiring data with HDF5 files.',
      author='Alan Sanders',
      author_email='alansanders89@gmail.com',
      url='https://www.gitlab.com/asanders89/datafile',
      packages=find_packages(),
      install_requires=['h5py'],
     )
